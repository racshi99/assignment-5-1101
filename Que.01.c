#include <stdio.h>

#define CostPerPerfomance 500
#define CostPerAttendee 3

int NoOfAttendees(int price);
int Income(int price);
int Cost(int price);
int Profit(int price);

int	NoOfAttendees(int price){
	return 120-(price-15)/5*20;
}


int Income(int price){
   return NoOfAttendees(price)*price;
}


int Cost(int price){
	return NoOfAttendees(price)*CostPerAttendee+CostPerPerfomance;
}


int Profit(int price){
   return Income(price)-Cost(price);
}

int main(){
	int price;
	printf("\nExpected Profit for Ticket Prices: \n\n");
	for(price=5;price<50;price+=5)
	{
		printf("Ticket Price = Rs.%d \t\t Profit = Rs.%d\n",price,Profit(price));
		printf("-----------------------------------------------------\n\n");
    }
		return 0;
}

